﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using System.Linq;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text.RegularExpressions;

public static class Utilities
{
	//private bool _isCoroutineExecuting = false;
	public static IEnumerator ExecuteAfterTime(float time, Action action){
		//if (_isCoroutineExecuting)
		//	yield break;
		//_isCoroutineExecuting = true;
		yield return new WaitForSeconds(time);
		action();
		//_isCoroutineExecuting = false;
	}

	/*
	 * Retrieval of GameObjects and Components
	 */

	public static List<GameObject> GetRootObjects(){
		List<GameObject> l = new List<GameObject> (); 
		UnityEngine.SceneManagement.SceneManager.GetActiveScene ().GetRootGameObjects (l);
		return l;
	}

	public static T GetRootComponentOfType<T>() where T : class{
		List<GameObject> rootObjects = GetRootObjects ();

		var rootObjectWithComponent = rootObjects.Find (go => ComponentExists (go.GetComponent<T> ()));

		return rootObjectWithComponent ? rootObjectWithComponent.GetComponent<T> () : null;
	}

	public static Option<T> TryGetRootComponentOfType<T>() where T : class{
		bool gameObjectExists = GetRootObjects ().Any(go => ComponentExists(go.GetComponent<T> ()));

		return gameObjectExists ? GetRootComponentOfType<T> ().ToOption () : Option.NoValue<T> ();
	}

	public static List<T> GetRootComponentsOfType<T>() {
		List<GameObject> rootObjects = GetRootObjects ();

		return rootObjects
			.Where (go => ComponentExists(go.GetComponent<T>()))
			.Select (go => go.GetComponent<T> ())
			.ToList ();
	}

	public static bool ComponentExists<T>(T component){
		return component != null && !component.Equals (null);
	}

	public static T GetComponentInDirectChildren<T>(this Component parent) where T : Component
	{
		return parent.GetComponentInDirectChildren<T>(false);
	}

	public static T GetComponentInDirectChildren<T>(this Component parent, bool includeInactive) where T : Component
	{
		foreach (Transform transform in parent.transform)
		{
			if (includeInactive || transform.gameObject.activeInHierarchy)
			{
				T component = transform.GetComponent<T>();
				if (component != null)
				{
					return component;
				}
			}
		}
		return null;
	}

	public static T[] GetComponentsInDirectChildren<T>(this Component parent) where T : Component
	{
		return parent.GetComponentsInDirectChildren<T>(false);
	}

	public static T[] GetComponentsInDirectChildren<T>(this Component parent, bool includeInactive) where T : Component
	{
		List<T> tmpList = new List<T>();
		foreach (Transform transform in parent.transform)
		{
			if (includeInactive || transform.gameObject.activeInHierarchy)
			{
				tmpList.AddRange(transform.GetComponents<T>());
			}
		}
		return tmpList.ToArray();
	}

	public static T GetComponentInSiblings<T>(this Component sibling) where T : Component
	{
		return sibling.GetComponentInSiblings<T>(false);
	}

	public static T GetComponentInSiblings<T>(this Component sibling, bool includeInactive) where T : Component
	{
		Transform parent = sibling.transform.parent;
		if (parent == null) return null;
		foreach (Transform transform in parent)
		{
			if (includeInactive || transform.gameObject.activeInHierarchy)
			{
				if (transform != sibling)
				{
					T component = transform.GetComponent<T>();
					if (component != null)
					{
						return component;
					}
				}
			}
		}
		return null;
	}

	public static T[] GetComponentsInSiblings<T>(this Component sibling) where T : Component
	{
		return sibling.GetComponentsInSiblings<T>(false);
	}

	public static T[] GetComponentsInSiblings<T>(this Component sibling, bool includeInactive) where T : Component
	{
		Transform parent = sibling.transform.parent;
		if (parent == null) return null;
		List<T> tmpList = new List<T>();
		foreach (Transform transform in parent)
		{
			if (includeInactive || transform.gameObject.activeInHierarchy)
			{
				if (transform != sibling)
				{
					tmpList.AddRange(transform.GetComponents<T>());
				}
			}
		}
		return tmpList.ToArray();
	}

	public static T GetComponentInDirectParent<T>(this Component child) where T : Component
	{
		Transform parent = child.transform.parent;
		if (parent == null) return null;
		return parent.GetComponent<T>();
	}

	public static T[] GetComponentsInDirectParent<T>(this Component child) where T : Component
	{
		Transform parent = child.transform.parent;
		if (parent == null) return null;
		return parent.GetComponents<T>();
	}

	/*
	 * File Management
	 */

	// static methods for writing and reading from files
	public static void WriteToFile(object obj, string name){
		BinaryFormatter bf = new BinaryFormatter ();
		FileStream fs = new FileStream (name, FileMode.OpenOrCreate);

		bf.Serialize (fs, obj);
		fs.Close ();
	}

	public static Option<T> LoadFromFile<T>(string name) where T : class {
		if (File.Exists (name)) {
			BinaryFormatter bf = new BinaryFormatter ();
			FileStream fs = new FileStream (name, FileMode.Open);
			T obj;

			obj = (T)bf.Deserialize (fs);

			fs.Close ();
			return obj.ToOption();
		}
		else {
			return Option.NoValue<T>();
		}
	}

	public static List<List<string>> LoadFromFileCSV(string name){
		if (File.Exists (name)) {
			List<List<string>> obj = new List<List<string>> ();

			using(var reader = new StreamReader(name))
			{
				while (!reader.EndOfStream)
				{				
					List<string> innerList = new List<string> ();
					var line = reader.ReadLine();
					var values = line.Split(',');

					innerList.Add(values[0]);
					innerList.Add(values[1]);

					obj.Add (innerList);

				}
			}

			return obj;
		}
		else {
			throw new NotImplementedException ("LoadFromFileCSV file not found.");
		}
	}

	public static List<string> LoadFromFileLines(string name) {
		if (File.Exists (name)) {
			List<string> obj = new List<string> ();

			using(var reader = new StreamReader(name))
			{
				while (!reader.EndOfStream)
				{				
					var line = reader.ReadLine();

					obj.Add (line);
				}
			}

			return obj;
		}
		else {
			throw new NotImplementedException ("LoadFromFileLines file not found.");
		}
	}

	public static string[] LoadFromResourceLines(TextAsset resource) {
		//var textFile = Resources.Load<TextAsset>(resourceName);
		string text = resource.text;

		var result = Regex.Split(text, "\r\n|\r|\n");

		return result;
	}

	public static List<List<string>> LoadFromResourceCSVWithDelAndCol(TextAsset resource, char del, int columnNr){
		List<List<string>> obj = new List<List<string>> ();

		using(var reader = new StringReader(resource.text))
		{
			var line = reader.ReadLine ();
			while (line != null)
			{				
				List<string> innerList = new List<string> ();
				var values = line.Split(del);

				for (int i = 0; i < columnNr; i++) {
					innerList.Add(values[i]);
				}

				obj.Add (innerList);
				line = reader.ReadLine ();
			}
		}

		return obj;
	}

	public static List<List<string>> LoadFromResourceCSVWithDel(TextAsset resource, char del){
		return LoadFromResourceCSVWithDelAndCol (resource, del, 2);
	}

	public static List<List<string>> LoadFromResourceCSV(TextAsset resource){
		return LoadFromResourceCSVWithDel (resource, ',');
	}

	/*
	 * Extension methods
	 */

	// Shuffle taken from: https://stackoverflow.com/questions/273313/randomize-a-listt
	public static System.Random RNG = new System.Random();  
	public static void Shuffle<T>(this IList<T> list)  
	{  
		int n = list.Count;  
		while (n > 1) {  
			n--;  
			int k = RNG.Next(n + 1);  
			T value = list[k];  
			list[k] = list[n];  
			list[n] = value;  
		}  
	}

	public static void Update<K,V>(this IDictionary<K,V> dictionary, K key, Func<V,V> updateFunc){
		dictionary [key] = updateFunc (dictionary [key]);
	}

	public static string Truncate(this string value, int maxLength)
	{
		if (string.IsNullOrEmpty(value)) return value;
		return value.Length <= maxLength ? value : value.Substring(0, maxLength); 
	}

	public static T PickRandom<T>(this IList<T> list){
		int n = list.Count;
		int randomIdx = RNG.Next (0, n - 1);

		return list[randomIdx];
	}

	public static Sprite ToSprite(this Texture2D tex){
		return Sprite.Create (tex, new Rect (0, 0, tex.width, tex.height), new Vector2 (0.5f, 0.5f));
	}

	public static Sprite GetSprite(this Toggle toggle) {
		return ((Image)toggle.targetGraphic).sprite;
	}

	// https://stackoverflow.com/questions/753316/extension-method-for-enumerable-intersperse
	public static IEnumerable<T> Intersperse<T>(this IEnumerable<T> source, T element)
	{
		bool first = true;
		foreach (T value in source)
		{
			if (!first) yield return element;
			yield return value;
			first = false;
		}
	}

	/*
	 * UI
	 */
	public static IEnumerator FadeGraphicToFullAlpha<T> (float t, T i, Action<T> onEndEffect) where T : Graphic
	{
		i.color = new Color(i.color.r, i.color.g, i.color.b, 0);
		while (i.color.a < 1.0f)
		{
			i.color = new Color(i.color.r, i.color.g, i.color.b, i.color.a + (Time.deltaTime / t));
			yield return null;
		}

		onEndEffect (i);
	}

	public static IEnumerator FadeGraphicToZeroAlpha<T> (float t, T i, Action<T> onEndEffect) where T : Graphic
	{
		i.color = new Color(i.color.r, i.color.g, i.color.b, 1);
		while (i.color.a > 0.0f)
		{
			i.color = new Color(i.color.r, i.color.g, i.color.b, i.color.a - (Time.deltaTime / t));
			yield return null;
		}

		onEndEffect (i);
	}

	public static Color WithAlpha (this Color color, float alpha){
		return new Color (color.r, color.g, color.b, alpha);
	}

	/* Others */

	// https://answers.unity.com/questions/61669/applicationopenurl-for-email-on-mobile.html
	public static void SendEmail (string address, string subject, string body)
	{
		string email = address;
		string escSubject = MyEscapeURL(subject);
		string escBody = MyEscapeURL(body);
		Application.OpenURL("mailto:" + email + "?subject=" + escSubject + "&body=" + escBody);
	}

	static string MyEscapeURL (string url)
	{
		return WWW.EscapeURL(url).Replace("+","%20");
	}
}

