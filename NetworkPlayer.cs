﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System;
using UnityEngine;
using UnityEngine.Networking;

public class NetworkPlayer : NetworkBehaviour {
	private static readonly string PlayerWantsToSkipQuestion = "{0} quer passar para outra pergunta!";
	private static readonly string OnlyPlayerLeft = "Oops.. eras o único jogador dentro do jogo.";

	[SyncVar]
	public string Name;
	public Option<Texture2D> Picture = Option.NoValue<Texture2D> ();

	public static NetworkPlayer LocalPlayer;
	public static NetworkPlayer Host;

	public Option<NetworkPlayer> PlayerThatCaughtMe;

	public string OwnAnswer;
	public int RemainingHints;
	public bool HasStarted = false;
	public int RoundNr;

	[SyncVar(hook="OnIsHostChanged")]
	public bool IsHost;

	// Use this for initialization
	void Start () {	
		DontDestroyOnLoad (this);

		if (isLocalPlayer) {
			CmdAddPlayerToGameManager ();
			HasStarted = true;
			RoundNr = 1;
		}

		// We need these two lines because on the clients, NetworkPlayer only starts *after* the playScene has loaded
		var questionCanvasController = Utilities.GetRootComponentOfType<QuestionCanvasController> ();
		if (Utilities.ComponentExists(questionCanvasController)){
			questionCanvasController.AddPlayer (this);
		}
			
		try{
			LobbyPlayer lobbyPlayer = 
				GameObject
					.FindObjectsOfType<LobbyPlayer>()
					.First(lp => lp.Name == Name);	
			SetPicture(lobbyPlayer.Picture);
		}
		catch (InvalidOperationException){
			Debug.LogError ("Could not find player... this should never happen.");
		}
	}
	
	// Update is called once per frame
	void Update () { 
	}

	void OnIsHostChanged(bool isHost){
		IsHost = isHost;
		if (isHost)
			Host = this;
	}

	public void SetPicture(byte[] data){
		if (data == null || data.Length == 0){
			Picture = Option.NoValue<Texture2D> ();
			return;
		}

		//deserialize data to object and do further things with it...
		Debug.LogWarningFormat ("Should be setting the picture!! Size: {0}",data.Length.ToString());
		Texture2D tex = new Texture2D (1,1); // size is irrelevant
		var success = tex.LoadImage (data);
		Picture = success ? tex.ToOption () : Option.NoValue<Texture2D> ();
		Debug.LogWarningFormat ("Name: {0} picture: {1} Image: {2}",Name,data.ToString(),Picture.ToString());
	}

	public override void OnStartLocalPlayer ()
	{
		base.OnStartLocalPlayer ();

		LocalPlayer = this;
		var lm = LobbyManager.s_LobbyManager;
		if (lm && lm.HostPlayer.HasValue ()){
			RemainingHints = lm.HostPlayer.ForceGetValue ().Hints;
			Debug.LogWarningFormat ("Remaining hints ");
		}
	}

	// SyncVars are guaranteed to be in sync before this function is called
	public override void OnStartClient ()
	{
		base.OnStartClient ();

		if (IsHost) {
			Host = this;
		}
	}

	public override void OnNetworkDestroy ()
	{
		base.OnNetworkDestroy ();

		if (!isLocalPlayer){
			RemovePlayer (gameObject);
		}

		// clean up UI refering to another player that disconnected
		if (NetworkServer.active && !isLocalPlayer && GameManager.GMSingleton && !GameManager.GMSingleton.GameEnded){
			GameManager.GMSingleton.RemovePlayerFromGame (this);
			if (GameManager.GMSingleton.NumberOfPlayers <= 1 && LobbyManager.s_LobbyManager){
				LobbyManager.s_LobbyManager.Abort (OnlyPlayerLeft);
				// TODO I dunno why but this line crashes the application
				//GameManager.GMSingleton.EndGame ();
			}
		}
	}

	public void RemoveAnswer(string answer){
		var choiceCanvasController = Utilities.TryGetRootComponentOfType<ChoiceCanvasController> ();
		choiceCanvasController.DoAction (choiceCanvas => choiceCanvas.RemovePlayerAnswer (answer));	
	}

	void RemovePlayer(GameObject npGameObject){
		var canvasControllers = Utilities.GetRootComponentsOfType<IGameCanvasController> ();
		canvasControllers.ForEach(cc => cc.RemovePlayer (npGameObject.GetComponent<NetworkPlayer> ()));		
	}

	/*
	 * Commands
	 */

	[Command]
	public void CmdGetCurrentQuestion(){
		GameManager.GMSingleton.GetCurrentQuestion (this);	
	}

	[Command]
	public void CmdQuestionReady(){
		GameManager.GMSingleton.QuestionReady (this);
	}
		
	[Command]
	public void CmdSubmitAnswer(string answer){
		GameManager.GMSingleton.SubmitAnswer (this, answer.ToOption (), true);
	}

	[Command]
	public void CmdTimeIsUp (string answer) {
		var optAnswer = (answer == null || answer == "") ? Option.NoValue<string> () : answer.ToOption ();

		GameManager.GMSingleton.SubmitAnswer (this, optAnswer, false);
	}

	[Command]
	public void CmdAddPlayerToGameManager(){
		GameManager.GMSingleton.AddPlayer (this);
	}

	[Command]
	public void CmdGetAnswers(){
		GameManager.GMSingleton.GetAnswers (this);
	}

	[Command]
	public void CmdSubmitChoice(string choice){
		GameManager.GMSingleton.SubmitChoice (this, choice);
	}

	[Command]
	public void CmdReadyForScores(){
		GameManager.GMSingleton.ReadyForScores (this);
	}

	[Command]
	public void CmdGetPlayerScores(){
		GameManager.GMSingleton.GetPlayerScores (this);
	}

	[Command]
	public void CmdReadyForNextRound(){
		GameManager.GMSingleton.SetReadyForNextRound (this);
	}

	[Command]
	public void CmdSkipQuestion(){
		GameManager.GMSingleton.SkipRound (this);
	}

	[Command]
	public void CmdSyncReady(){
		GameManager.GMSingleton.PlayerReady (this);
	}

	[Command]
	public void CmdGetHint(){
		GameManager.GMSingleton.GetHint (this);
	}

	/*
	 * RPCs
	 */

	[ClientRpc] // QuestionScene
	public void RpcPlayerAnswerSubmitted(){
		var questionCanvasController = Utilities.GetRootComponentOfType<QuestionCanvasController> ();

		if (!Utilities.ComponentExists(questionCanvasController))
			return;

		if (isLocalPlayer) {
			// TODO check if case sensitive
			OwnAnswer = questionCanvasController.AnswerInputField.text.ToLower ();
			questionCanvasController.AnswerSubmitted ();
		}
		questionCanvasController.SetReady (this);
	}

	[ClientRpc] // ChoiceScene
	public void RpcPlayerChoiceSubmitted(){
		var choiceCanvasController = Utilities.GetRootComponentOfType<ChoiceCanvasController> ();
		if (Utilities.ComponentExists (choiceCanvasController))
			choiceCanvasController.AnswerSubmitted (this);
	}
		
	[ClientRpc] // AnyScene
	public void RpcRemoveAnswer(string answer){
		RemoveAnswer (answer);
	}

	[ClientRpc] // QuestionScene
	public void RpcPlayerSkipRound(){
		if (isLocalPlayer)
			return;

		string msg = string.Format(PlayerWantsToSkipQuestion, Name);
		NotificationManager.s_NotificationManager.Notify (msg);
	}

	[ClientRpc] // CaughtScene
	public void RpcSyncEnded (){
		// need countdown controller here...
		CountdownCanvasController countdownCanvas = Utilities.GetRootComponentOfType<CountdownCanvasController> ();

		if (Utilities.ComponentExists(countdownCanvas))
			countdownCanvas.StartAnimation ();
	}

	[ClientRpc]
	public void RpcReadyForNextRound (){
		var scoreCanvasController = Utilities.GetRootComponentOfType<ScoreCanvasController> ();

		if (scoreCanvasController){
			// TODO do something here
			scoreCanvasController.SetPlayerReadyForNextRound (this);
		}
	}

	[TargetRpc] // QuestionScene
	public void TargetChangeQuestion(NetworkConnection target, string question, int roundNr){
		var questionCanvasController = Utilities.GetRootComponentOfType<QuestionCanvasController> ();

		RoundNr = roundNr;
		
		if (Utilities.ComponentExists(questionCanvasController))
			questionCanvasController.StartUp (question);
	}

	[TargetRpc] // ChoiceScene
	public void TargetAnswers(NetworkConnection target, string currentQuestion, string[] answers){
		var choiceCanvasController = Utilities.GetRootComponentOfType<ChoiceCanvasController> ();
		var lm = LobbyManager.s_LobbyManager;

		if (Utilities.ComponentExists(choiceCanvasController) && Host && LocalPlayer && lm && lm.HostPlayer.HasValue())
			choiceCanvasController.ClearAndPopulate (currentQuestion, answers, LocalPlayer.RoundNr, lm.HostPlayer.ForceGetValue().Rounds, LocalPlayer.RemainingHints);
	}

	[TargetRpc] // ChoiceScene (-> CaughtScene)
	public void TargetPlayerThatCaughtMe(NetworkConnection target, GameObject npGameObject){
		PlayerThatCaughtMe = npGameObject 
			? npGameObject.GetComponent<NetworkPlayer> ().ToOption()
			: Option.NoValue<NetworkPlayer>();
	}

	[TargetRpc] // ScoreScene
	public void TargetPlayerScoresAndRounds(NetworkConnection target, 
		GameObject[] players, int[] scores, int[] deltas, 
		GameObject[] playersWithAnswers, string[] answers, int[] noPicks, 
		string correctAnswer, int noCorrectPicks,
		GameObject[] caughtPlayers){

		var resultsCanvasController = Utilities.GetRootComponentOfType<ResultsCanvasController> ();
		var scoreCanvasController = Utilities.GetRootComponentOfType<ScoreCanvasController> ();	

		var elapsedRounds = NetworkPlayer.LocalPlayer ? NetworkPlayer.LocalPlayer.RoundNr : 0;
		var numberOfRounds = LobbyManager.s_LobbyManager ? LobbyManager.s_LobbyManager.HostPlayer.Fold (hp => hp.Rounds, 0) : 0;

		if (Utilities.ComponentExists(resultsCanvasController)){
			resultsCanvasController.SetRoundNumber (elapsedRounds, numberOfRounds);
			resultsCanvasController.PopulateResults (playersWithAnswers, answers, noPicks, correctAnswer, noCorrectPicks, caughtPlayers);	
		}

		if (Utilities.ComponentExists(scoreCanvasController)){
			scoreCanvasController.SetRoundNumber (elapsedRounds, numberOfRounds);
			scoreCanvasController.PopulateScores (players, scores, deltas);	
		}
	}

	[TargetRpc] // QuestionScene
	public void TargetInvalidAnswer(NetworkConnection target, string message){
		var questionCanvasController = Utilities.GetRootComponentOfType<QuestionCanvasController> ();
			
		if (Utilities.ComponentExists (questionCanvasController))
			questionCanvasController.InvalidAnswer (message);
	}
		
	[TargetRpc] // ChoiceScene
	public void TargetHint(NetworkConnection target, string wrongAnswer){
		var choiceCanvasController = Utilities.GetRootComponentOfType<ChoiceCanvasController> ();

		if (Utilities.ComponentExists(choiceCanvasController) && NetworkPlayer.LocalPlayer){
			NetworkPlayer.LocalPlayer.RemainingHints--;
			choiceCanvasController.RemovePlayerAnswer (wrongAnswer); // TODO show notification?
			choiceCanvasController.Hints = NetworkPlayer.LocalPlayer.RemainingHints;
		}
	}
}
