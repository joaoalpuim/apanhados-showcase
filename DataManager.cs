﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System;
using UnityEngine;
using Data;

public class DataManager : MonoBehaviour {
	private static string StorePath;
	private const int NameMaxCharacters = 12;

	public static DataManager s_DataManagerSingleton;

	public static Data.Player DefaultPlayer { get { return new Player ("", new byte[] {}, false); } }

	public static T GetKey<T>(string key, Func<string, T> getFunc, T def) {
		return PlayerPrefs.HasKey (key) ? getFunc(key) : def;
	}

	public static int GetKey(string key, int def){
		return GetKey<int> (key, PlayerPrefs.GetInt, def);
	}
	public static string GetKey(string key, string def){
		return GetKey<string> (key, PlayerPrefs.GetString, def);
	}
	public static float GetKey(string key, float def){
		return GetKey<float> (key, PlayerPrefs.GetFloat, def);
	}
	public static bool GetKey(string key, bool def){
		return GetKey<bool> (key, str => PlayerPrefs.GetInt(str) == 0 ? false : true, def);
	}

	public static void SetKey(string key, bool value){
		PlayerPrefs.SetInt (key, value ? 1 : 0);
	}

	[HideInInspector]
	public Player Player { 
		get { return LoadPlayer (); }
		set { Save (value); }
	}

	void Awake (){
		if (s_DataManagerSingleton != null){
			Destroy (gameObject);
			return;
		}

		DontDestroyOnLoad (gameObject);
		s_DataManagerSingleton = this;
		StorePath = Application.persistentDataPath;
	}
		
	void Start () {}
	void Update () {}

	public void SetName(string name) {
		Player player = Player;
		player.Name = name;
		Save (player);
	}

	public void SetPicture(byte[] pictureData){
		Player player = Player;
		player.Picture = pictureData;
		Save (player);
	}

	private void Save(Player player){
		Utilities.WriteToFile (player, Path.Combine(StorePath, "PlayerInfo.dat"));
	}

	private Player LoadPlayer(){
		try {
			Data.Player player = Utilities.LoadFromFile<Data.Player>(Path.Combine(StorePath, "PlayerInfo.dat")).OrElse(DefaultPlayer);
			player.Name = player.Name.Truncate(NameMaxCharacters);
			return player;
		} catch (Exception) {
			return DefaultPlayer;
		}
	}
}
